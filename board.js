var score =0;
var type= -1;
var interval ;
var highScore = localStorage.getItem('highScore');
var onloadCall = function(){
  const highScoreContainer = document.querySelector('#updateHighScore'); 
        highScoreContainer.innerHTML= highScore;  
} 
function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}
removeBackGroundColor= function(maxNumber){
     for(let i=0;i<maxNumber;i++){
        const container = document.querySelector(`#col${i}`);  
        container.style.backgroundColor='lightGray'; 
       }
}
function removeInterval(interval){
clearInterval(interval);
removeBackGroundColor(type);
}

var startTimer =function (time, inter, size){
    interval = setInterval(() => {
       const maxNumber = size*size;
      removeBackGroundColor(maxNumber);
    const id = Math.floor(Math.random()*maxNumber);
    const randomCell = document.querySelector(`#col${id}`);
    randomCell.style.backgroundColor = 'green';
   },inter*1000);
  setTimeout(()=>{removeInterval(interval)},time*1000);  
}
function resetGame(){
    score = 0;
    const container = document.querySelector('#updateScore'); 
   container.innerHTML= score;
   removeInterval(interval)
   startTimer(120,1, type)
}
function updateScore(ev){
    const currentCell = document.querySelector(`#${ev.target.id}`);
   
    if(currentCell.style.backgroundColor =='green'){
         ++score;
    } else {
         --score;
    }
    let highScore = localStorage.getItem('highScore');
    if(highScore< score){
        localStorage.setItem('highScore', score);
        const highScoreContainer = document.querySelector('#updateHighScore'); 
        highScoreContainer.innerHTML= score;
    }
   const container = document.querySelector('#updateScore'); 
   container.innerHTML= score;
}

var changeGameSize =function (size){
    type= size;
     const container = document.querySelector('#displayGame');
        removeAllChildNodes(container);
        let id =0;
    for(let i =0; i<size;i++){
        const row = document.createElement('div');
        row.setAttribute('id',`row${i}`)
        row.classList.add('game-row')
        for(let j=0;j<size;j++){
            const col=document.createElement('div');
            col.setAttribute('id',`col${id}`)
            col.style.cursor = 'pointer';
            id++;
             col.classList.add('game-columns')
             col.addEventListener('click', updateScore);
            row.appendChild(col);
        }
        container.appendChild(row)
    }
    startTimer(120,1, size)
}
onloadCall();